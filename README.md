# UiPath Chapter Colombia - Rest API Workshop

## Instalación

```bash
$ npm install
```

## Ejecutar el proyecto

```bash
# desarrollo
$ npm run start

# desarrollo con recarga automática
$ npm run start:dev

# modo producción
$ npm run start:prod
```