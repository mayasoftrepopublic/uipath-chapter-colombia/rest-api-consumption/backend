export class UserDto {
  nombre: string;
  cedula: string;
  edad: number;
  activo: boolean;
  infoContacto: {
    direccion: string;
    telefonos: string[];
  };
}
