import {
  Controller,
  Get,
  Post,
  Body,
  Headers,
  HttpException,
  HttpStatus,
  Param,
  Patch,
} from '@nestjs/common';
import { ExamplesService } from './examples.service';
import { GenerateTokenDto } from './dto/generate-token.dto';
import { UserDto } from './dto/user.dto';

@Controller('examples')
export class ExamplesController {
  constructor(private readonly examplesService: ExamplesService) {}

  @Get('usuarios')
  getJArray(): any {
    return this.examplesService.getUsers();
  }

  @Get('usuarios/:userId')
  getJObject(@Param('userId') userId: string): any[] {
    return this.examplesService.getUser(userId);
  }

  @Post('crear-usuario')
  createUser(@Headers() headers, @Body() userDto: UserDto) {
    this.examplesService.validateBasicAuth(headers);
    return this.examplesService.createUser(userDto);
  }

  @Post('generar-token-oauth')
  generateOauthToken(@Body() generateTokenDto: GenerateTokenDto) {
    return this.examplesService.generateOauthToken(generateTokenDto);
  }

  @Patch('actualizar-info')
  async updateUser(@Headers() headers, @Body() userDto: UserDto) {
    const userId = this.examplesService.validateToken(headers);
    console.log(userId);
    return this.examplesService.updateUser(userId, userDto);
  }
}
