import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { UserDto } from './dto/user.dto';
import { GenerateTokenDto } from './dto/generate-token.dto';
// import * as JWTGenerator from 'jwt-generator';

@Injectable()
export class ExamplesService {
  users = {
    '1095867923': {
      nombre: 'Alejandro López',
      cedula: '1095867923',
      edad: 29,
      activo: true,
      infoContacto: {
        direccion: 'Calle 10 No. 9 - 78',
        telefonos: ['3156229812', '3008491829'],
      },
    },
    '1097846728': {
      nombre: 'Samuel Hernández',
      cedula: '1097846728',
      edad: 27,
      activo: false,
      infoContacto: {
        direccion: 'Calle 20 No. 22 - 27 piso 3',
        telefonos: ['3047280111', '3010839335'],
      },
    },
    '1003387732': {
      nombre: 'Lyda Zúñiga',
      cedula: '1003387732',
      edad: 22,
      activo: true,
      infoContacto: {
        direccion: 'Carrera 8a No. 7 - 88',
        telefonos: ['3217172829'],
      },
    },
    '38017852': {
      nombre: 'Luz Marina Pinto',
      cedula: '38017852',
      edad: 46,
      activo: true,
      infoContacto: {
        direccion: 'Calle 12 No. 4 - 19',
        telefonos: ['3004128943', '3170178927', '3002198446', '3004001287'],
      },
    },
    '1098722382': {
      nombre: 'Julian Valderrama',
      cedula: '1098722382',
      edad: 26,
      activo: true,
      infoContacto: {
        direccion: 'Carrera 22 No. 17-31',
        telefonos: [],
      },
    },
  };

  constructor(
    private configService: ConfigService,
    private jwtService: JwtService,
  ) {}

  getUser(userId: string): any {
    if (this.users[userId]) {
      return this.users[userId];
    }
    throw new HttpException('El usuario no existe', HttpStatus.NOT_FOUND);
  }

  getUsers(): any[] {
    return Object.values(this.users);
  }

  createUser(userDto: UserDto) {
    if (this.users[userDto.cedula]) {
      throw new HttpException('El usuario ya existe', HttpStatus.CONFLICT);
    } else {
      this.users[userDto.cedula] = userDto;
    }
  }

  updateUser(userId: string, userDto: UserDto) {
    if (this.users[userId]) {
      this.users[userId]['nombre'] = userDto.nombre
        ? userDto.nombre
        : this.users[userId]['nombre'];
      this.users[userId]['edad'] = userDto.edad
        ? userDto.edad
        : this.users[userId]['edad'];
      this.users[userId]['activo'] = userDto.activo
        ? userDto.activo
        : this.users[userId]['activo'];
      this.users[userId]['infoContacto']['direccion'] =
        userDto.infoContacto && userDto.infoContacto.direccion
          ? userDto.infoContacto.direccion
          : this.users[userId]['infoContacto']['direccion'];
      this.users[userId]['infoContacto']['telefonos'] =
        userDto.infoContacto && userDto.infoContacto.telefonos
          ? userDto.infoContacto.telefonos
          : this.users[userId]['infoContacto']['telefonos'];
    } else {
      throw new HttpException('El usuario no existe', HttpStatus.NOT_FOUND);
    }
  }

  generateOauthToken(generateTokenDto: GenerateTokenDto) {
    const tokenData = {
      iss: 'https://demos.mayasoft.ai',
      exp: Math.floor(new Date().getTime() / 1000) + 3600,
      sub: generateTokenDto.cedula,
      aud: 'https://uipath-chapter.com.co/',
      scope: 'read:resources',
    };

    const token = this.jwtService.sign(tokenData);

    return {
      tokenData,
      token,
    };
  }

  validateBasicAuth(headers: any) {
    const authString = headers['authorization'].split(' ')[1];
    const credentials = Buffer.from(authString, 'base64').toString('ascii');
    const [username, password] = credentials.split(':');
    if (
      username != this.configService.get('BASIC_AUTH_USER') ||
      password != this.configService.get('BASIC_AUTH_PASS')
    ) {
      throw new HttpException(
        'Credenciales de autenticación inválidas',
        HttpStatus.UNAUTHORIZED,
      );
    }
    return authString;
  }

  validateToken(headers: string): string {
    if (!headers['authorization']) {
      throw new HttpException(
        'El servicio requiere un token de autorización',
        HttpStatus.UNAUTHORIZED,
      );
    }

    const token = headers['authorization'].split(' ')[1];
    let decoded;

    try {
      decoded = this.jwtService.decode(token);
      console.log(decoded);
      if (!decoded) {
        throw new HttpException(
          'El token no tiene el formato correcto',
          HttpStatus.UNAUTHORIZED,
        );
      }
    } catch (error) {
      throw new HttpException(
        'El token no tiene el formato correcto',
        HttpStatus.UNAUTHORIZED,
      );
    }

    if (decoded['iss'] != 'https://demos.mayasoft.ai') {
      throw new HttpException('Token inválido', HttpStatus.UNAUTHORIZED);
    }
    if (decoded['exp'] < Math.floor(new Date().getTime() / 1000)) {
      throw new HttpException('Token expirado', HttpStatus.UNAUTHORIZED);
    }

    // If token is valid, return client's id
    return decoded['sub'];
  }
}
